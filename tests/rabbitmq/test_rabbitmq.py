import unittest
import uuid

from openpatch_core.rabbitmq import rabbit


class TestRabbitMQ(unittest.TestCase):
    def test_publish_uuid(self):
        test = {"id": uuid.uuid4()}
        rabbit.publish_json(test, "test")


