import unittest
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from flask_sqlalchemy import SQLAlchemy
from flask_testing import TestCase
from sqlalchemy.orm import sessionmaker, relationship
from flask import Flask
from openpatch_core.database.elastic_query import ElasticQuery
from openpatch_core.models import Base
from openpatch_core.database import db
from sqlalchemy.dialects import mysql


class City(Base):
    __tablename__ = "city"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    users = relationship("User", back_populates="city")

    def __repr__(self):
        return str(self.id)


class UserColor(Base):
    __tablename__ = "user_color"

    user_id = Column(Integer, ForeignKey("users.id"), primary_key=True)
    color_id = Column(Integer, ForeignKey("colors.id"), primary_key=True)

    user = relationship("User", back_populates="user_colors")
    color = relationship("Color", back_populates="user_colors")


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    lastname = Column(String)
    uid = Column(Integer)
    city_id = Column(Integer, ForeignKey(City.id))
    city = relationship(City, back_populates="users")

    user_colors = relationship("UserColor", back_populates="user")

    def __repr__(self):
        return str(self.id)


association_art_color = Table(
    "art_color",
    Base.metadata,
    db.Column("art_id", Integer, ForeignKey("arts.id")),
    db.Column("color_id", Integer, ForeignKey("colors.id")),
)


class Color(Base):
    __tablename__ = "colors"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    user_colors = relationship("UserColor", back_populates="color")
    arts = relationship("Art", secondary=association_art_color, back_populates="colors")


class Art(Base):
    __tablename__ = "arts"

    id = Column(Integer, primary_key=True)
    name = Column(String)

    colors = relationship(
        "Color", secondary=association_art_color, back_populates="arts"
    )


class ElasticQueryTest(TestCase):
    def create_app(self):
        app = Flask(__name__)
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
        app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()
        db.session.add_all(
            [
                User(name="Jhon", lastname="Galt", uid="19571957", city_id=1),
                User(name="Steve", lastname="Jobs", uid="20092009", city_id=2),
                User(name="Iron", lastname="Man", uid="19571957", city_id=1),
                City(name="Cordoba"),
                City(name="New York"),
                Color(name="red"),
                Color(name="blue"),
                UserColor(user_id=1, color_id=1),
                UserColor(user_id=1, color_id=2),
                UserColor(user_id=2, color_id=2),
                Art(name="Starry Night"),
                Art(name="Mona Lisa"),
            ]
        )
        db.session.commit()

        art1 = Art.query.get(1)
        art1.colors.append(Color.query.get(1))
        art2 = Art.query.get(2)
        art2.colors.append(Color.query.get(2))

        db.session.commit()

    def test_setup_is_ok(self):
        """ Demo test """
        assert User.query.count() == 3

    def test_simple_query(self):
        """ test simple query """
        query_string = '{"filter" : {"uid" : {"like" : "%1957%"} } }'
        query, count, page = User.elastic_query(query_string)
        assert query.count() == 2
        assert count == 2
        query_string = (
            '{"filter" : {"name" : {"like" : "%Jho%"}, "lastname" : "Galt" } }'
        )
        query, count, page = User.elastic_query(query_string)
        assert query.count() == 1
        assert count == 1

    def test_limit_operator(self):
        query_string = '{"limit": 2}'
        query, count, page = User.elastic_query(query_string)
        query = query = page()
        assert query.count() == 2
        assert count == User.query.count()

    def test_offset_operator(self):
        query_string = '{"offset": 2}'
        query, count, page = User.elastic_query(query_string)
        query = page()
        assert query.count() == 1
        assert count == User.query.count()

        query_string = '{"limit": 2, "offset": 2}'
        query, count, page = User.elastic_query(query_string)
        query = page()
        assert query.count() == 1
        assert count == User.query.count()

    def test_and_operator(self):
        """ test and operator """
        query_string = '{"filter" : {"and" : {"name" : {"like" : "%Jho%"}, "lastname" : "Galt", "uid" : {"like" : "%1957%"} } } }'
        query, count, page = User.elastic_query(query_string)
        assert query.count() == 1
        assert count == 1

    def test_or_operator(self):
        """ test or operator """
        query_string = '{"filter" : {"or" : { "name" : "Jobs", "lastname" : "Man", "uid" : "19571957" } } }'
        query, count, page = User.elastic_query(query_string)
        assert query.count() == 2
        assert count == 2

    def test_or_and_operator(self):
        """ test or and operator """
        query_string = '{"filter" : {"or" : { "name" : "Jhon", "lastname" : "Galt" }, "and" : { "uid" : "19571957" } } }'
        query, count, page = User.elastic_query(query_string)
        assert query.count() == 1
        assert count == 1

    def test_sorting(self):
        """ test operator levels """
        query_string = '{"filter" : {"or" : { "name" : "Jhon", "lastname" : "Man" } }, "sort": { "name" : "asc" } }'
        results = User.elastic_query(query_string)[0].all()
        assert results[0].name == "Iron"

    def test_in_operator(self):
        """ test operator in """
        query_string = '{"filter" : {"name" : {"in" : ["Jhon", "Peter", "Iron"] } } }'
        assert User.elastic_query(query_string)[0].count() == 2

        query_string = '{"filter" : {"name" : {"in" :["Jhon", "Peter", "Iron"]}, "lastname" : "Galt" } }'
        assert User.elastic_query(query_string)[0].count() == 1

    def test_allow_fields_option(self):
        """ test allow_fields option """
        query_string = '{"filter" : {"or" : { "name" : "Jhon", "lastname" : "Man" } }, "sort": { "name" : "asc" } }'
        enabled_fields = ["name"]
        results = User.elastic_query(query_string, enabled_fields=enabled_fields)[
            0
        ].all()
        assert results[0].name == "Jhon"

    def test_search_for_levels(self):
        """ test search for levels """
        query_string = '{"filter" : {"or" : { "city.name" : "New York", "lastname" : "Man" } }, "sort": { "name" : "asc" } }'
        results = User.elastic_query(query_string)[0].all()
        assert results[0].name == "Iron"

        query_string = '{"filter" : { "city.name" : "New York" } }'
        results = User.elastic_query(query_string)[0].all()
        assert results[0].name == "Steve"

        query_string = '{"filter" : { "city.name" : {"like" : "%New%"} } }'
        query = User.elastic_query(query_string)
        results = query[0].all()
        assert results[0].name == "Steve"

    def test_many_to_many_relationship(self):
        query_string = '{"filter" : { "user_colors.color.name" : "red" } }'
        query = User.elastic_query(query_string)
        results = query[0].all()

        assert len(results) == 1

        query_string = '{"filter" : { "user_colors.color.name" : "blue" } }'
        query = User.elastic_query(query_string)
        results = query[0].all()

        assert len(results) == 2

        query_string = '{"filter": { "colors.name": "blue" }}'
        query = Art.elastic_query(query_string)

        results = query[0].all()
        assert len(results) == 1

    def test_many_to_many_relationship_deep(self):
        query_string = (
            '{"filter" : { "user_colors.color.arts.name" : "Starry Night" } }'
        )
        query = User.elastic_query(query_string)
        results = query[0].all()

        assert len(results) == 1
        assert results[0].name == "Jhon"

    def test_one_to_many_relationship(self):
        query_string = '{"filter" : { "users.name" : "Jhon" } }'
        query = City.elastic_query(query_string)
        results = query[0].all()

        assert len(results) == 1


def print_query(query):
    print(
        query[0].statement.compile(
            dialect=mysql.dialect(), compile_kwargs={"literal_binds": True}
        )
    )
