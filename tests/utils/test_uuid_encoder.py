import unittest
import json
import uuid
from openpatch_core.utils.uuid_encoder import UUIDEncoder


class TestUUIDEncoder(unittest.TestCase):
    def test_json_dumps(self):
        test = {"id": uuid.uuid4()}

        res = json.dumps(test, cls=UUIDEncoder)
        self.assertIn("id", res)

        load = json.loads(res)
        self.assertIn("id", load)

        id = uuid.UUID(load["id"])
        self.assertEqual(test["id"], id)
