import unittest
import base64
import json
import werkzeug
from unittest.mock import MagicMock, patch
from openpatch_core.jwt import _decode_jwt_from_headers, get_jwt_claims


def make_fake_headers(jwt_claims):
    token_template = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.%s.F3OLwaYV5mKgus82Soo2tE_ninlwlkg-5XtRt2njgu8"
    json_payload = json.dumps(
        {"user_claims": jwt_claims}, separators=(",", ":")
    ).encode("utf-8")
    encoded_token = (
        base64.urlsafe_b64encode(json_payload).replace(b"=", b"").decode("utf-8")
    )
    headers = {"Authorization": token_template % encoded_token}
    return headers


class TestJWT(unittest.TestCase):
    def test_decode_jwt_from_headers(self):
        jwt = _decode_jwt_from_headers(make_fake_headers({"id": 1}))
        self.assertEqual(jwt, {"user_claims": {"id": 1}})
        jwt = _decode_jwt_from_headers(None)
        self.assertIsNone(jwt)
        jwt = _decode_jwt_from_headers({"Authorization": ""})
        self.assertIsNone(jwt)
        jwt = _decode_jwt_from_headers({"Authorization": None})
        self.assertIsNone(jwt)

    def test_get_jwt_claims(self):
        m = MagicMock()
        m.headers = make_fake_headers({"id": 1})
        with patch("openpatch_core.jwt.request", m):
            jwt = get_jwt_claims()
            self.assertEqual(jwt, {"id": 1})

        m.headers = None
        with patch("openpatch_core.jwt.request", m):
            with self.assertRaises(werkzeug.exceptions.Unauthorized):
                jwt = get_jwt_claims()
                self.assertEqual(jwt, {"id": 1})

        m.headers = {"Authorization": ""}
        with patch("openpatch_core.jwt.request", m):
            with self.assertRaises(werkzeug.exceptions.Unauthorized):
                jwt = get_jwt_claims()
                self.assertEqual(jwt, {"id": 1})

        m.headers = {"Authorization": None}
        with patch("openpatch_core.jwt.request", m):
            with self.assertRaises(werkzeug.exceptions.Unauthorized):
                jwt = get_jwt_claims()
                self.assertEqual(jwt, {"id": 1})
