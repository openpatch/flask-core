import setuptools

__version__ = "0.0.0"

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="openpatch_core",
    version=__version__,
    author="Mike Barkmin",
    author_email="mbarkmin@gmail.com",
    packages=setuptools.find_packages(),
    description="flask core for openpatch",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://openpatch.gitlab.io/flask-core",
    install_requires=[
        "Flask-Migrate>=2.5.2",
        "Flask-SQLAlchemy>=2.4.1",
        "Flask>=1.1.1",
        "SQLAlchemy>=1.3.12",
        "flask-marshmallow>=0.10.1",
        "marshmallow-sqlalchemy==0.24.1",
        "mysqlclient>=1.4.6",
        "pika==1.1.0",
        "requests>=2.22.0",
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
