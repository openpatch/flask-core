FROM python:3.8

COPY . /usr/app

WORKDIR /usr/app

ENV OPENPATCH_AUTHENTIFICATION_SERVICE="https://api.openpatch.app/authentification"

RUN pip3 install -r requirements.txt
