# Environment Variables

- OPENPATCH_SERVICE_NAME
- OPENPATCH_AUTHENTIFICATION_SERVICE

# Build
```
docker build -t flask-core .
```

# Test

```
docker run -v "$PWD:/usr/app" flask-core python3 -m unittest
docker run -v "$PWD:/usr/app" flask-core python3 -m unittest tests.database.test_elastic_query
```
