from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.database.elastic_query import ElasticQuery


class Base(db.Model):
    __abstract__ = True

    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

    @classmethod
    def elastic_query(cls, query, enabled_fields=[]):
        instance = ElasticQuery(cls, query, enabled_fields)
        return instance.search()


class Member(Base):
    __abstract__ = True
    __tablename__ = gt("member")

    id = db.Column(GUID(), primary_key=True)
    username = db.Column(db.String(64))
    avatar_id = db.Column(GUID())
    full_name = db.Column(db.String(128))

    @classmethod
    def get_or_create(cls, jwt):
        member = cls.query.get(jwt.get("id"))

        if not member:
            member = cls(id=jwt.get("id"))

        member.username = jwt.get("username")
        member.avatar_id = jwt.get("avatar_id")
        member.full_name = jwt.get("full_name")

        db.session.commit()
        return member
