import base64
import json
import os
from functools import wraps

import requests

from flask import abort, request

# Adaption of the jwt extended library for flask
try:
    from flask import _app_ctx_stack as ctx_stack
except ImportError:  # pragma: no cover
    from flask import _request_ctx_stack as ctx_stack


def jwt_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        base_url = os.getenv("OPENPATCH_AUTHENTIFICATION_SERVICE")
        if base_url:
            headers = {"Authorization": request.headers.get("Authorization", None)}
            url = "%s/v1/verify" % base_url
            r = requests.get(url, headers=headers)
            if r.status_code != 200:
                abort(401)
        return func(*args, **kwargs)

    return wrapper


def jwt_roles_required(roles):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            jwt_claims = get_jwt_claims()
            role = jwt_claims.get("role")
            if role not in roles:
                abort(403)
            return func(*args, **kwargs)

        return wrapper

    return decorator


def get_jwt_claims(optional=False):
    headers = request.headers
    try:
        jwt = _decode_jwt_from_headers(headers)
    except:
        jwt = None
    if jwt is None and optional:
        return None
    elif jwt is None:
        abort(401)
    return jwt.get("user_claims")


def _decode_jwt_from_headers(headers):
    if not headers:
        return None
    jwt_header = headers.get("Authorization", None)

    if not jwt_header:
        return None

    parts = jwt_header.split()
    jwt = parts[1].split(".")
    claims = jwt[1]
    return json.loads(base64url_decode(claims).decode("utf-8"))


def base64url_decode(input):
    if isinstance(input, str):
        input = input.encode("ascii")

    rem = len(input) % 4

    if rem > 0:
        input += b"=" * (4 - rem)

    return base64.urlsafe_b64decode(input)
