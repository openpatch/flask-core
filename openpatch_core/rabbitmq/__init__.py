import os
import pika
import json
import uuid
import time
import logging
import threading

from openpatch_core.utils.uuid_encoder import UUIDEncoder


def mock_message(message):
    logging.info(f"MOCK: {message}")


class RabbitMQ:
    def __init__(self):
        self.rabbitmq_host = os.getenv("RABBITMQ_HOST")
        if not self.rabbitmq_host:
            logging.info("No RABBITMQ_HOST configured use mock")
        self._connection = None
        self._channel = None
        self._data = {}

    def connect(self):
        if not self.rabbitmq_host:
            mock_message(f"Connecting to mock")
            return
        params = pika.URLParameters(self.rabbitmq_host)
        self._connection = pika.BlockingConnection(params)
        self._channel = self._connection.channel()

    def close(self):
        if not self.rabbitmq_host:
            mock_message("Close connection")
            return
        self._connection.close()

    def temporary_queue_declare(self):
        if not self.rabbitmq_host:
            mock_message("Declare temporary queue")
            return
        return self.queue_declare(exclusive=True, auto_delete=True)

    def queue_declare(
        self,
        queue_name="",
        passive=False,
        durable=False,
        exclusive=False,
        auto_delete=False,
        arguments=None,
    ):
        if not self.rabbitmq_host:
            mock_message(f"Declare queue {queue_name}")
            return
        result = self._channel.queue_declare(
            queue=queue_name,
            passive=passive,
            durable=durable,
            exclusive=exclusive,
            auto_delete=auto_delete,
            arguments=arguments,
        )
        return result.method.queue

    def accept(self, corr_id, result):
        mock_message(f"Accept {corr_id} {result}")
        self._data[corr_id]["isAccept"] = True
        self._data[corr_id]["result"] = json.loads(result)
        self._channel.queue_delete(self._data[corr_id]["reply_qeue_name"])

    def on_response(self, ch, method, props, body):
        logging.info("on response => {}".format(body))

        corr_id = props.correlation_id
        self.accept(corr_id, body)

    def publish(self, body, exchange="", routing_key=None, corr_id=None, durable=False):
        if not self.rabbitmq_host:
            mock_message(
                f"Publish {body} to exchange {exchange} with routing_key {routing_key} and corr_id {corr_id}"
            )
            return
        self.connect()
        self._channel.exchange_declare(exchange=exchange, durable=durable)

        if not corr_id:
            self._channel.basic_publish(
                exchange=exchange, routing_key=routing_key, body=body
            )
        else:
            self._channel.basic_publish(
                exchange=exchange,
                routing_key=routing_key,
                body=body,
                properties=pika.BasicProperties(correlation_id=corr_id),
            )
        self.close()

    def publish_json(
        self, body, exchange="", routing_key=None, corr_id=None, durable=False
    ):
        data = json.dumps(body, cls=UUIDEncoder)
        self.publish(
            data,
            exchange=exchange,
            routing_key=routing_key,
            corr_id=corr_id,
            durable=durable,
        )

    def publish_sync(self, body, exchange="", routing_key=None, timeout=5):
        if not self.rabbitmq_host:
            mock_message(
                f"Publish sync {body} to exchange {exchange} with routing_key {routing_key}"
            )
            return
        corr_id = str(uuid.uuid4())
        callback_queue = self.temporary_queue_declare()
        self._data[corr_id] = {
            "isAccept": False,
            "result": None,
            "reply_qeue_name": callback_queue,
        }

        self._channel.basic_consume(callback_queue, self.on_response, no_ack=True)
        self._channel.basic_publish(
            exchange=exchange,
            routing_key=routing_key,
            body=body,
            properties=pika.BasicProperties(
                reply_to=callback_queue, correlation_id=corr_id
            ),
        )

        end = time.time() + timeout

        while time.time() < end:
            if self._data[corr_id]["isAccept"]:
                logging.info("Got the RPC server response")
                return self._data[corr_id]["result"]
            else:
                self._connection.process_data_events()
                time.sleep(0.3)
        logging.error("RPC timeout")
        return None

    def publish_json_sync(self, body, exchange="", routing_key=None):
        data = json.dumps(body, cls=UUIDEncoder)
        return self.publish_sync(data, exchange=exchange, routing_key=routing_key)

    def publish_email(self, email, subject, body):
        self.publish_json(
            {"version": 1, "to": email, "subject": subject, "text": body},
            exchange="mail_exchange",
            routing_key="mail_queue",
            durable=True,
        )

    def publish_activity(self, type, resource, member):
        self.publish_json(
            {"version": 1, "type": type, "resource": resource, "member": member},
            exchange="activity_exchange",
            routing_key="activity_queue",
            durable=True,
        )


rabbit = RabbitMQ()
