from flask import jsonify
from werkzeug.exceptions import HTTPException, default_exceptions


class HTTPError:
    def __init__(self, msg, status_code):
        self.msg = msg
        self.status_code = status_code

    def to_response(self):
        return jsonify({
            'msg': self.msg,
            'status_code': self.status_code
        }), self.status_code


class ErrorManager:
    """
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "405: Method Not Allowed", "status": 405, "code": 0 }

    Also provides app specific errors, which have a unique code and
    could have detailed information:

    {
      "message": "Your credentials do not allow access to this resource",
      "status": 403,
      "code": 1
    }
    """

    def make_json_error(self, status, message=None, code=None, details=None):
        response = jsonify(
            message=message, status=status, code=code, details=details)
        response.status_code = status
        return response

    def make_default_json_error(self, ex):
        status = (ex.code if isinstance(ex, HTTPException) else 500)
        return self.make_json_error(status, message=str(ex), code=0)

    def init_app(self, app):
        for code, v in default_exceptions.items():
            app.error_handler_spec[None] = {}
            app.error_handler_spec[None][code] = self.make_json_error


error_manager = ErrorManager()
